use bincode;
use serde_yaml;
use std;
use std::path::Path;

use crate::localcommand::LocalCommand;
use crate::gossip::*;
use crate::crypto::PubKey;

pub fn send_cmd(command: &LocalCommand) {
	use std::io::Write;
	use std::fs::OpenOptions;

	let fifo_path = &std::path::Path::new("/tmp/gossip-fifo");

	let mut file = OpenOptions::new().write(true).open(fifo_path).unwrap();
	file.write_all(&*bincode::serialize(command).unwrap()).unwrap();
}

pub fn identity_from_config_file(config_path: &Path) -> Result<PubKey, std::io::Error> {
	let mut file = std::fs::File::open(&config_path)?;

	use std::io::Read;
	let mut s = String::new();
	file.read_to_string(&mut s)?;//.expect("failed to read configuration");

	let g: Gossiper = serde_yaml::from_str(&*s)
		.map_err(|e|
			std::io::Error::new(std::io::ErrorKind::InvalidInput, e.to_string()))?; //.expect("failed to parse configuration")
	Ok(g.identity)
}
