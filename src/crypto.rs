extern crate hex;
extern crate serde;
extern crate sodiumoxide;

use std;
use self::serde::{Deserialize, Deserializer};

use self::sodiumoxide::crypto::box_::curve25519xsalsa20poly1305::*;

pub type Nonce = self::sodiumoxide::crypto::box_::curve25519xsalsa20poly1305::Nonce;

#[derive(PartialEq, Debug, Clone, Serialize)]
pub struct PrivKey(#[serde(with = "hex")]
SecretKey);

#[derive(PartialEq, Copy, Clone, Serialize)]
pub struct PubKey(#[serde(with = "hex")]
PublicKey);

fn deserialize_hex<'de, D, T, F>(deserializer: D, slice_to_opt: F) -> Result<T, D::Error>
	where D: Deserializer<'de>, F: FnOnce(&[u8]) -> Option<T> {
	use self::hex::FromHex;
	let hex: String = Deserialize::deserialize(deserializer)?;
	/* may not be valid hex */
	let data: Vec<u8> = match FromHex::from_hex(hex) {
		Ok(h) => h,
		Err(_) => return Err(serde::de::Error::invalid_value(serde::de::Unexpected::Other("invalid hex"), &"hex-encoded key")),
	};
	/* may not be right # of bytes */
	slice_to_opt(&*data).map(Ok)
		.unwrap_or_else(|| Err(serde::de::Error::invalid_length(data.len(), &"correct number of encoded bytes")))
}

impl<'de> Deserialize<'de> for PubKey {
	fn deserialize<D>(deserializer: D) -> Result<PubKey, D::Error>
		where D: Deserializer<'de>
	{
		deserialize_hex(deserializer, |x| PublicKey::from_slice(x).map(PubKey))
	}
}

impl<'de> Deserialize<'de> for PrivKey {
	fn deserialize<D>(deserializer: D) -> Result<PrivKey, D::Error>
		where D: Deserializer<'de>
	{
		deserialize_hex(deserializer, |x| SecretKey::from_slice(x).map(PrivKey))
	}
}

impl std::str::FromStr for PubKey {
	type Err=serde::de::value::Error;
	fn from_str(hex: &str) -> Result<PubKey, serde::de::value::Error> {
	use self::hex::FromHex;

	let data: Vec<u8> = match FromHex::from_hex(hex) {
		Ok(h) => h,
		Err(_) => return Err(serde::de::Error::invalid_value(serde::de::Unexpected::Other("invalid hex"), &"hex-encoded key")),
	};

	/* may not be right # of bytes */
	PublicKey::from_slice(&*data).map(PubKey).map(Ok)
		.unwrap_or_else(|| Err(serde::de::Error::invalid_length(data.len(), &"correct number of encoded bytes")))
	}
}

impl std::fmt::Debug for PubKey {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let bytes = &self.0[..];
		write!(f, "{:x}{:x}{:x}{:x}{:x}…", bytes[0], bytes[1], bytes[2], bytes[3], bytes[4])
	}
}

pub fn gen_nonce() -> Nonce {
	let mut x = [0; NONCEBYTES];
	sodiumoxide::randombytes::randombytes_into(&mut x[..]);
	Nonce::from_slice(&x[..]).expect("failed to create nonce")
}

pub fn gen_keys() -> (PubKey, PrivKey) {
	let (pubkey, privkey) = gen_keypair();
	(PubKey(pubkey), PrivKey(privkey))
}

pub fn decode<'a, I: Iterator<Item=&'a PubKey>>(pubkeys_iter: I, privkey: &PrivKey, encoded: &[u8]) -> Option<(PubKey, Vec<u8>)> {
	assert!(sodiumoxide::init().is_ok());

	if encoded.len() < NONCEBYTES {
		return None
	}

	let (nonce, ciphertext) = encoded.split_at(NONCEBYTES);

	let nonce = match Nonce::from_slice(nonce) {
		Some(n) => n,
		None => return None,
	};

	for pubkey in pubkeys_iter {
		match open(ciphertext, &nonce, &pubkey.0, &privkey.0) {
			Ok(data) => return Some((*pubkey, data)),
			Err(()) => continue,
		}
	}
	None
}

pub fn encode(target: &PubKey, privkey: &PrivKey, nonce: &mut Nonce, plaintext: &[u8]) -> Vec<u8> {
	assert!(sodiumoxide::init().is_ok());

	nonce.increment_le_inplace();

	let ciphertext = seal(plaintext, &*nonce, &target.0, &privkey.0);

	let mut out = nonce[..].to_vec();
	out.extend(ciphertext);
	out
}
