#![feature(conservative_impl_trait)]

#[macro_use]
extern crate serde_derive;

extern crate bincode;
extern crate bytes;
extern crate futures;
extern crate rand;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_timer;
extern crate serde_yaml;

use std::io;
use tokio_core::reactor::Core;

mod gossip;
mod crypto;
mod lamport_time;
mod localcommand;
mod member;
mod memberlist;
mod namedpipe;
mod route;

use gossip::*;

fn usage() -> ! {
	println!("usage: hm03 [--] CONFIG.YAML");
	std::process::exit(1);
}

fn main() {
	let mut args = std::env::args_os();
	let _exe = args.next();
	let path = match args.next() {
		Some(path) => path,
		None => usage(),
	};
	//if path.starts_with("-") { usage() }

	let mut g = match std::fs::File::open(&path) {
		Ok(mut f) => {
			use std::io::Read;
			let mut s = String::new();
			f.read_to_string(&mut s).expect("failed to read configuration");
			serde_yaml::from_str(&*s).expect("failed to parse configuration")
		},
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
			let addr = "[::]:2115".parse().expect("invalid local address");
			let (pubkey, privkey) = crypto::gen_keys();
			Gossiper::new(pubkey, privkey, addr)
		},
		Err(e) => {
			println!("error opening configuration: {}", e);
			std::process::exit(1)
		},
	};

	g.config_path = Some(path);
	let _ = save_config(&g);

	let mut event_loop = Core::new().expect("failed to create tokio core");

	g.run(&mut event_loop);
}
