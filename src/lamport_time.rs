use std;

#[derive(Clone, PartialEq, PartialOrd, Serialize, Deserialize, Default)]
pub struct LamportTime(usize);

impl LamportTime {
	pub fn new() -> Self { LamportTime(0) }

	pub fn max(&self, other: &LamportTime) -> LamportTime {
		LamportTime(std::cmp::max(self.0, other.0))
	}
}

impl std::fmt::Debug for LamportTime {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "t{}", self.0)
	}
}
