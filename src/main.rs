#![feature(async_closure)]
#![allow(clippy::let_and_return)]

#[macro_use]
extern crate serde_derive;

extern crate bincode;
extern crate bytes;
extern crate futures;
extern crate ip_watcher;
extern crate rand;
extern crate tokio;
extern crate tokio_stream;
extern crate serde_yaml;
extern crate structopt;

mod gossip;
mod crypto;
mod lamport_time;
mod localcommand;
mod member;
mod memberlist;
mod message;
mod namedpipe;
mod route;
mod cmd;

use crypto::PubKey;
use localcommand::LocalCommand;
use std::net::SocketAddr;
use std::path::Path;
use structopt::StructOpt;

#[derive(StructOpt)]
enum Action {
	#[structopt(name = "daemon", about = "run the gossip daemon")]
	Daemon {},
	#[structopt(name = "connect-file", about = "connect to a peer given its config file and an address for it")]
	AddPeerFile { peer_config: String, addr: SocketAddr },
	#[structopt(name = "connect", about = "connect to a peer given its public key and address")]
	AddPeer { peer: PubKey, addr: SocketAddr },
	#[structopt(name = "set", about = "set a gossip parameter")]
	SetParam { param: String, val: String },
	#[structopt(name = "get", about = "read a gossip parameter")]
	GetParam { param: String, },
	#[structopt(name = "resolve", about = "print a peer's current addresses")]
	Resolve { peer: PubKey },
	#[structopt(name = "save", about = "save the current network state to config file")]
	SaveConfig {},
	#[structopt(name = "list-peers", about = "list the active peers of the current network state")]
	ListPeers {},
}

#[derive(StructOpt)]
#[structopt(name = "hm03", about = "a serf-inspired gossip network tracking machine reachability")]
struct Opts {
	#[structopt(name = "CONFIG_PATH", help = "configuration path")]
	config_path: String,
	#[structopt(subcommand)]
	action: Action,
}

fn main() {
	let opts = Opts::from_args();

	let config_path = Path::new(&opts.config_path);

	let cmd = match opts.action {
		Action::Daemon {} => return gossip::main(config_path),
		Action::AddPeerFile { peer_config, addr } => {
			let identity = cmd::identity_from_config_file(Path::new(&*peer_config)).expect("could not obtain identity from config file");
			LocalCommand::AddPeer(identity, addr)
		},
		Action::AddPeer { peer, addr } => LocalCommand::AddPeer(peer, addr),
		Action::SetParam { param, val } => LocalCommand::SetParam(param, val),
		Action::GetParam { param } => LocalCommand::GetParam(param),
		Action::Resolve { peer } => LocalCommand::Resolve(peer),
		Action::SaveConfig {} => LocalCommand::SaveConfig,
		Action::ListPeers {} => LocalCommand::ListPeers,
	};
	cmd::send_cmd(&cmd)
}
