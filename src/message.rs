use crate::crypto::PubKey;
use crate::lamport_time::LamportTime;
use crate::memberlist::MemberList;

/** messages sent across the network */
#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub enum Message {
	MemberListChunk(MemberList), /* members each have own lamport time? */
	Sync(SyncMessage),
}

/** messages updating relationship between peers */
#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
pub struct SyncMessage {
	pub kind: MessageKind,
	pub subject: PubKey,
	pub object: PubKey,
	pub seqno: LamportTime,
}

/** member relationship actions */
#[derive(Serialize, Deserialize, PartialEq, Copy, Clone, Debug)]
pub enum MessageKind {
	Pings,
	RepliesTo,
	CouldNotReach,
	Suspects,
	Leaves,
}
