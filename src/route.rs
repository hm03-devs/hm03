use std;
use std::net::SocketAddr;
use crate::lamport_time::LamportTime;

#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Route {
	pub addr: SocketAddr,
	/** if the local node has been able to contact this address */
	pub reachable: bool,
	pub last_updated: LamportTime,
}


impl std::fmt::Debug for Route {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{}{}@{:?}", self.addr, if self.reachable { "+" } else { "" }, self.last_updated)
	}
}

impl Route {
	pub fn new(addr: SocketAddr, last_updated: LamportTime) -> Route {
		Route {
			addr,
			reachable: false,
			last_updated,
		}
	}
}
