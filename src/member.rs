use crate::crypto::PubKey;
use crate::lamport_time::LamportTime;
use crate::route::Route;
use crate::message::{MessageKind, SyncMessage};

use std;
use std::net::SocketAddr;

/** a representation of a queued message to a given member */
#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct MessageIntent {
	/** subject of action */
	subject: PubKey,
	/** object of action */
	object: PubKey,
	/** the timestamp of this message */
	seqno: LamportTime,
	/** how many times sending this message should be retried */
	ttl: u8,
}

/** the messages queud for sending to a member */
#[derive(Serialize, Deserialize, PartialEq, Clone, Default)]
pub struct SendIntents
{
	/** ping from subject to object */
	pub pings_intent: Option<MessageIntent>,
	/** pong in response to a subject -> object ping */
	pub repliesto_intent: Option<MessageIntent>,
	/** reponse when subject -> object ping was successful */
	pub peer_reachable_intent: Option<MessageIntent>,
	/** response when subject -> object ping timed out */
	pub couldnotreach_intent: Option<MessageIntent>,
	/** subject suspects object */
	pub suspects_intent: Option<MessageIntent>,
}

/** a gossip peer, which may be reached from any number of addresses,
not all of which may be accessible from the local network */
#[derive(Serialize, Deserialize, PartialEq, Clone)]
pub struct Member {
	/** public key that signs messages to prove they came from this peer */
	pub identity: PubKey,
	/** when a ping was last sent to this member.
	used as the base for suspicion timeout and indirect probe timeout */
	pub last_ping_sent: u64,
	#[serde(skip)]
	/** messages queued for sending to this member */
	pub send_intents: SendIntents,
	/** captures some notion of staleness (?); may be redundant with `routes[i].heartbeat` */
	pub heartbeat: u32,
	/** sorted list of locations at which this peer may be reachable */
	pub routes: Vec<Route>,
}

impl std::fmt::Debug for Member {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(f, "{:?}=>{:?}", self.identity, self.routes)
	}
}

impl Member {
	/** create a new member with no routes */
	pub fn new(identity: PubKey) -> Member {
		Member {
			identity,
			last_ping_sent: 0,
			send_intents: SendIntents::default(),
			heartbeat: 0,
			routes: vec![],
		}
	}

	/** update a member's routes in response to new info */
	pub fn seen_with_addr(&mut self, addr: SocketAddr, time: LamportTime) {
		if let Some(route) = self.routes.iter_mut().find(|r| r.addr==addr) {
			route.last_updated = route.last_updated.max(&time);
			return
		}
		self.routes.push(Route::new(addr, time));
	}

	/** update routes to this peer based on a message,
	joining the peer's state vector with the local one, */
	pub fn merge_routes(&mut self, routes: &[Route]) {
		for r in routes {
			self.seen_with_addr(r.addr, r.last_updated.clone())
		}
	}

	pub fn get_random_route(&self) -> Option<&Route> {
		use rand::seq::SliceRandom;
		self.routes.choose(&mut rand::thread_rng())
	}

	/** get the length of time after this member's most recent message send
	when this member will next be ready to send a message.
	returns or zero if no message is pending on a timeout */
	pub fn next_timeout_duration(&self) -> usize {
		0
	}

	/** returns true if the message was queued,
	false if an existing message in the queue took higher priority */
	pub fn queue_message(&mut self, kind: MessageKind, subject: PubKey, object: PubKey, seqno: LamportTime) -> bool
	{
		use crate::message::MessageKind::*;
		let maybe_intent = match kind {
			Pings => &mut self.send_intents.pings_intent,
			RepliesTo => &mut self.send_intents.repliesto_intent,
			CouldNotReach => &mut self.send_intents.couldnotreach_intent,
			Suspects => &mut self.send_intents.suspects_intent,
			Leaves => return false,
		};
		/* TODO: perform prioritization based on ttl */
		/*let old_existed = if let ref Some(intent) = maybe_intent {
			ttl > 5
		}*/
		*maybe_intent = Some(MessageIntent {
			subject,
			object,
			seqno,
			ttl: 5,
		});
		true
	}

	/** generate a message by consuming the appropriate send intent */
	fn generate_message(&mut self, kind: MessageKind) -> Option<SyncMessage>
	{
		use crate::message::MessageKind::*;
		let maybe_intent = match kind {
			Pings => &mut self.send_intents.pings_intent,
			RepliesTo => &mut self.send_intents.repliesto_intent,
			CouldNotReach => &mut self.send_intents.couldnotreach_intent,
			Suspects => &mut self.send_intents.suspects_intent,
			Leaves => return None,
		};
		if let Some(ref mut intent) = *maybe_intent {
			if intent.ttl > 0 {
				intent.ttl -= 1;
				Some(SyncMessage {
					kind,
					subject: intent.subject,
					object: intent.object,
					seqno: intent.seqno.clone(),
				})
			} else {
				None
			}
		} else {
			None
		}
	}

	/** get the next message to send. decrements the TTL of the given message */
	pub fn pop_message(&mut self) -> Option<SyncMessage> {
		macro_rules! try_kind {
			($k: path) => {
				if let Some(message) = self.generate_message($k) {
					return Some(message)
				}
			}
		}
		use crate::message::MessageKind::*;
		try_kind!(Leaves);
		try_kind!(RepliesTo);
		try_kind!(CouldNotReach);
		try_kind!(Pings);
		try_kind!(Suspects);
		None
	}
}
