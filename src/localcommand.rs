use crate::crypto::PubKey;
use std::io;
use std::net::SocketAddr;
use bytes::BytesMut;
use tokio_util::codec::{/*Encoder, */Decoder};
use bincode;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum LocalCommand {
	/** set a named gossip parameter to a given value */
	AddPeer(PubKey, SocketAddr),
	/** set a named gossip parameter to a given value */
	SetParam(String, String),
	/** get the value of a named gossip parameter */
	GetParam(String),
	/** get the address of a given peer */
	Resolve(PubKey),
	/** save the configuration */
	SaveConfig,
	/** list pubkeys of all known peers */
	ListPeers,
}

/*#[derive(Clone, Debug, Serialize)]
pub enum LocalResponse {
	/** get the address of a given peer */
	PeerInfo(PubKey, SocketAddr),
	/** the value of a named gossip parameter */
	ParamValue(String, String),
}*/

pub struct LocalCommandCodec;

impl Decoder for LocalCommandCodec {
	 type Item = LocalCommand;
	 type Error = io::Error;

	 fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<LocalCommand>> {
		//if let Some(i) = buf.iter().position(|&b| b == b'\n') {
			// remove the serialized frame from the buffer.
			//let line = buf.split_to(i);

			// Also remove the '\n'
			//buf.split_to(1);

			match bincode::deserialize(&buf) {
				Ok(v) => {
					buf.clear();
					Ok(Some(v))
				},
				Err(e) => { eprintln!("error decoding local command: {}", e); Ok(None) },
			}
			//}.map(Some).map_err(|e|
			//	io::Error::new(io::ErrorKind::Other, e));
			//buf.clear();
			//out
		//} else {
		//	Ok(None)
		//}
	}
}

/*impl Encoder for LocalCommandCodec {
	 type Item = LocalResponse;
	 type Error = io::Error;

	 fn decode(&mut self, buf: &mut BytesMut) -> io::Result<Option<String>> {
		if let Some(i) = buf.iter().position(|&b| b == b'\n') {
			// remove the serialized frame from the buffer.
			let line = buf.split_to(i);

			// Also remove the '\n'
			buf.split_to(1);

			// Turn this data into a UTF string and return it in a Frame.
			match str::from_utf8(&line) {
				 Ok(s) => Ok(Some(s.to_string())),
				 Err(_) => Err(io::Error::new(io::ErrorKind::Other, "invalid UTF-8")),
			}
		} else {
			Ok(None)
		}
	}
}*/
