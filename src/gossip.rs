use std;
use std::default::Default;
use std::io;
use std::net::SocketAddr;
use std::sync::{Arc, Mutex};
use std::time::Duration;

use futures::{future, Future};
use futures::sink::SinkExt;
use futures::stream::{Stream, StreamExt, TryStreamExt};
use tokio::runtime::Runtime;
use tokio::net::{UdpSocket, TcpListener, TcpStream};
use tokio_util::codec::{length_delimited, FramedRead};
use tokio_util::udp::UdpFramed;
use tokio_stream::wrappers::{IntervalStream, TcpListenerStream};

use crate::crypto::{self, PubKey, PrivKey, Nonce};
use crate::lamport_time::LamportTime;
use crate::localcommand::{self, LocalCommand};
use crate::member::Member;
use crate::message::{Message, SyncMessage, MessageKind};
use crate::memberlist::MemberList;
use crate::namedpipe;

/** numerical parameters for the gossip protocol */
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Params {
	/** fan-out: how many random addresses are sent a probe request each round */
	k: usize,
	/** message ttl */
	rounds: usize,
	/** probability of storing message */
	alpha: f32,
	/** time between gossip rounds */
	delta: Duration,
	/** time before counting a node as failed */
	fail_time: Duration, /* TODO: use to mark nodes as failed! */
	/** time between full state transfers over TCP */
	state_exchange_delta: Duration,
}

impl Default for Params {
	fn default() -> Params {
		Params {
			k: 10,
			rounds: 3,
			alpha: 0.5,
			delta: Duration::from_secs(1),
			fail_time: Duration::from_secs(10),
			state_exchange_delta: Duration::from_secs(10),
		}
	}
}

/** the dynamically-mutated protocol state for a Gossiper */
#[derive(Debug, Serialize, Deserialize)]
pub struct State {
	#[serde(skip, default = "crypto::gen_nonce")]
	send_nonce: crypto::Nonce,
	lamport_time: LamportTime,
	member_list: MemberList,
	dead_list: MemberList,
}

impl State {
	/** update own state in response to received message.
	this includes queuing responses */
	fn update(&mut self, local_id: PubKey, from_id: PubKey, msg: &Message) {
		use self::Message::*;
		use crate::message::MessageKind::*;
		#[allow(unused_variables)]

		match *msg {
			MemberListChunk(ref members) => {
				/*destructively union own member list with the list in a message*/
				self.member_list.merge(members);
				let mut list = MemberList::new();
				/* also use the `from` address */
				if let Some(from) = self.member_list.get_mut(from_id) {
					list.add(from.clone());
				} else {
					eprintln!("could not find sender of pong");
				}
				self.member_list.merge(&list);
			},
			Sync(ref sm) => match sm.kind {
				Pings => {
					if sm.object == local_id {
						/* someone pinged us; queue pong both directly to originator
						(e.g. if this is a delegated ping) and back via sender */
						if let Some(from) = self.member_list.get_mut(from_id) {
							from.queue_message(RepliesTo, sm.subject, sm.object, sm.seqno.clone());
						} else {
							eprintln!("could not find sender of pong");
						}
						/* if  */
						if sm.object != from_id {
							if let Some(pinger) = self.member_list.get_mut(sm.object) {
								pinger.queue_message(RepliesTo, sm.subject, sm.object, sm.seqno.clone());
							} else {
								eprintln!("could not queue pong for unknown peer {:?}", sm.subject);
							}
						}
					} else {
						/* not for us; forward */
						if let Some(to) = self.member_list.get_mut(sm.subject) {
							to.queue_message(Pings, sm.subject, sm.object, sm.seqno.clone());
						} else {
							eprintln!("could not find target of ping to forward");
						}
					}
				},
				RepliesTo => {
					/* update seen time */
					if let Some(ponger) = self.member_list.get_mut(sm.subject) {
						//ponger.seen_with_addr(socketaddr, sm.seqno)
						//don't have addr? TODO: deal w/ later if necessary
					} else {
						eprintln!("pong for unknown peer {:?}", sm.subject);
					}
					/* if someone else originated this, forward the pong */
					if sm.object != local_id {
						if let Some(pinger) = self.member_list.get_mut(sm.object) {
							pinger.queue_message(RepliesTo, sm.subject, sm.object, sm.seqno.clone());
						} else {
							eprintln!("pong cannot be forwarded to unknown peer {:?}", sm.object);
						}
					}
				},
				CouldNotReach => {
					/* if we tried to reach someone and couldn't, ... */
					if sm.object == local_id {
						unimplemented!();
					} else {
						unimplemented!();
					}
					/* cancel expected ack? update suspicion? */
				},
				Suspects => {
					/* update suspicion */
				},
				Leaves => {
					/* forget member? move to dead list? */
				},
			},
		};
	}
}

/** a running participant in a gossip protocol */
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Gossiper {
	pub identity: PubKey,
	pub privkey: PrivKey,
	pub source_addr: SocketAddr,
	pub params: Params,
	#[serde(with = "state")]
	pub state: Arc<Mutex<State>>,
	#[serde(skip)]
	pub config_path: Option<std::path::PathBuf>,
}

mod state {
	extern crate serde;
	use self::serde::*;
	use std::sync::{Arc, Mutex};
	use super::State;

	pub fn serialize<S>(state: &Arc<Mutex<State>>, ser: S) -> Result<S::Ok, S::Error>
		where S: Serializer {
		let s = state.lock().unwrap();
		Serialize::serialize(&*s, ser)
	}

	pub fn deserialize<'de, D>(des: D) -> Result<Arc<Mutex<State>>, D::Error>
		where D: Deserializer<'de> {
		Deserialize::deserialize(des).map(|s| Arc::new(Mutex::new(s)))
	}
}

type MessageReceipt = (Option<SocketAddr>, PubKey, Message);

/** which side of a full-state exchange to perform */
enum ExchangeSide {
	/** initiator side of exchange simply chooses a target, connects, then
	sends that peer a signed, encrypted memberlist */
	Initiator(PubKey, SocketAddr),
	/** acceptor side of exchange is triggered by a connection attempt, but
	it does not know from whom, so it initially reads a message and deduces
	peer identity from message signature */
	Acceptor(TcpStream),
}

impl Gossiper {
	pub fn new(identity: PubKey, privkey: PrivKey, addr: SocketAddr) -> Gossiper {
		let mut member = Member::new(identity);
		member.seen_with_addr(addr, LamportTime::new());

		Gossiper {
			identity,
			privkey,
			state: Arc::new(Mutex::new(State {
				send_nonce: crypto::gen_nonce(),
				member_list: MemberList::new_from(Some(member)),
				dead_list: MemberList::new(),
				lamport_time: LamportTime::new(),
			})),
			source_addr: addr,
			params: Default::default(),
			config_path: None,
		}
	}

	pub fn source_addr(&self) -> &SocketAddr {
		&self.source_addr
	}

	/** run a gossip protocol.

	the gossip protocol is based off of SWIM and the modifications made by Serf,
	but modifies these to avoid conflating logical nodes with NICs.

	SWIM gives the insight that membership update dissemination (gossip) and
	failure detection should be considered separate but interacting tasks.

	failure detection follows serf by using "nack" for probe requests and
	keeping a dead-node cache, as well as the dynamic per-node suspicion timeout.

	care must be taken to distinguish a node as a whole going down from changes
	in an address's local reachability.
	*/
	pub fn run(&mut self, core: &mut Runtime) {
		let _guard = core.enter();

		/* handle local commands */
		let command_stream = self.open_localcommand_stream()
			.expect("could not open command fifo");
		core.spawn(self.listen_localcommands(command_stream));

		/* handle network messages */
		let socket = core.block_on(UdpSocket::bind(&self.source_addr()))
			.expect("failed to bind local port");

		let (udp_sink, udp_messages) = UdpFramed::new(socket, BinaryCodec(self.privkey.clone(), Arc::clone(&self.state))).split();
		/* incorporate socketaddr into tuple */
		let udp_messages = udp_messages.map_ok(move |x| (Some(x.1), (x.0).0, (x.0).1));

		/* await the bind() call */
		let tcp_listener = core.block_on(TcpListener::bind(&self.source_addr))
			.expect("could not bind listener");

		let tcp_messages = self.receive_tcp_connections(tcp_listener)
			.filter_map(async move |x| x.ok())
			.map(|x| x.ok_or(std::io::Error::new(std::io::ErrorKind::Other, "tcp message err")));

		/* occasionally send full state to random peer */
		let tcp_listen_messages = core.block_on(self.periodic_state_exchange())
			.filter_map(async move |x| x.ok())
			.map(|x| x.ok_or(std::io::Error::new(std::io::ErrorKind::Other, "tcp listen message err")));

		use std::pin::Pin;
		type MsgStream = Pin<Box<dyn Stream<Item=Result<(Option<SocketAddr>, PubKey, Message), io::Error>>>>;

		let messages_stream = futures::stream::select_all(vec![
			Box::pin(udp_messages) as MsgStream,
			Box::pin(tcp_messages) as MsgStream,
			Box::pin(tcp_listen_messages) as MsgStream,
		]);

		/* perform heartbeat */
		/*core.spawn(udp_sink.send_all(self.produce_pings()).then(|_| Ok(())));*/
		let produce_pings = core.block_on(self.produce_pings());
		core.spawn(produce_pings.map_ok(move |x| (x.clone(), x.0)).forward(udp_sink));
		//.with(async move |x : ((SocketAddr, PubKey, Message, PrivKey), _)| x.0)));

		/* interpret gossip messages */
		core.block_on(self.serve(Box::new(messages_stream)))
	}

	fn receive_tcp_connections(&mut self, listener: TcpListener) -> impl Stream<
		Item=Result<Option<MessageReceipt>, io::Error>> + 'static
	{
		let state = Arc::clone(&self.state);
		let privkey = self.privkey.clone();
		let listener_stream = TcpListenerStream::new(listener);
		listener_stream
			.filter_map(move |x| futures::future::ready(x.ok())) // Item = TcpStream
			.then(move |stream| {
			let addr = stream.peer_addr().expect("could not get peer address for connection");
			let state = Arc::clone(&state);
			let privkey = privkey.clone();
			println!("tcp connection from {}", addr);
			use futures::future::TryFutureExt;
				Gossiper::exchange_memberlist(state, privkey, ExchangeSide::Acceptor(stream))
				/* discard addr because TCP remote has junk port # */
				.map_ok(move |tup| tup.map(|(_addr, pubkey, msg)| (None, pubkey, msg)))
			//) as impl Future<Output=Result<Option<MessageReceipt>, io::Error>>
		})
	}

	async fn produce_pings(&mut self) -> impl Stream<Item=Result<(SocketAddr, PubKey, Message, PrivKey), io::Error>> {
		/*let state_stream = stream::unfold(Arc::clone(&self.state), |state| {
			//let () = state;
			Some(future::ok((Arc::clone(&state), state)))
		});*/

		let timer_ticks = IntervalStream::new(tokio::time::interval(self.params.delta));

		let k = self.params.k;
		let identity = self.identity;
		let privkey = self.privkey.clone();
		let state = Arc::clone(&self.state);
		let send_pings = timer_ticks /*.zip(state_stream)*/
			.map(move |_now| {
				let (member_list, seqno) = {
					let state = state.lock().expect("could not lock state mutex");
					(state.member_list.clone(), state.lamport_time.clone())
				};
				let privkey = privkey.clone();
				
				/* select `k` targets to ping */
				let targets: Vec<_> = member_list.get_random_members(&identity, k).into_iter().cloned()
					.filter_map(|m| match m.get_random_route() {
					Some(r) => Some((m.identity, r.addr)),
					None => None,
				}).collect();
				
				/* turn targets into a stream */
				let targets_stream = futures::stream::iter(targets.into_iter());
				
				/* create a stream of the addrs and messages to send them */
				let addrs_messages_stream = targets_stream.map(move |(target_id, addr)| {
					let privkey = privkey.clone();
					println!("Sending ping to {:?} @ {}", target_id, addr);
					(addr, target_id, Message::Sync(SyncMessage {
					kind: MessageKind::Pings,
					subject: identity,
					object: target_id,
					seqno: seqno.clone(),
					/*members: member_list,*/}), privkey)
				});
				addrs_messages_stream
			});
		send_pings.flatten().map(Ok)
	}

	/** initiate or respond to a memberlist exchange. see `ExchangeSide` enum. */
	fn exchange_memberlist(state: Arc<Mutex<State>>,
		privkey: PrivKey, side: ExchangeSide)
		-> impl Future<Output=Result<Option<MessageReceipt>, std::io::Error>> {

		/* construct full-state sync message */
		let member_list = state.lock().expect("could not lock state mutex").member_list.clone();
		let msg = Message::MemberListChunk(member_list);
		let state = Arc::clone(&state);

		/* wtb: session types */
		let do_exchange = match side {
			/* send peer message, read response */
			ExchangeSide::Initiator(target_id, addr) => {
				println!("initiating full state exchange: {:?} @ {}", target_id, addr);
				let stream_fut = TcpStream::connect(addr);
				/* send, receiving as soon as possible */
				let initiator_exchange_fut = async move {
					let mut stream = match stream_fut.await {
						Ok(s) => s,
						Err(e) => return Err(e),
					};
					let (read, write) = stream.split();
					let read = length_delimited::Builder::new().new_read(read);
					let mut write = length_delimited::Builder::new().new_write(write);

					let encoded_msg = encode_message(&msg, &target_id,
						&privkey, &mut state.lock().unwrap().send_nonce);

					write.send(encoded_msg.into()).await?;
					println!("sent fse message");
					let (buf, _) = read.into_future().await;
					let buf = buf.transpose()?;

					let list = &state.lock().unwrap().member_list;
					let pubkey_msg : Result<_, io::Error> = decode_message(&*buf.unwrap_or_default(), &privkey, &list);
					println!("decoded fse reply");
					pubkey_msg.map(|(pubkey, msg)| -> Option<MessageReceipt> {
						Some((None, pubkey, msg))
					})
				};
				future::Either::Left(initiator_exchange_fut)
			},
			/* read message and determine sender identity, then construct and send reply */
			ExchangeSide::Acceptor(mut stream) => {
				println!("accepting full state exchange");
				let acceptor_exchange_fut = async move {
					let (read, write) = stream.split();
					let read = length_delimited::Builder::new().new_read(read);
					let mut write = length_delimited::Builder::new().new_write(write);

					/* receive message */
					let (buf, _) = read.into_future().await;
					let buf = buf.transpose()?;
					let read = {
						let list = &state.lock().unwrap().member_list;
						println!("decoded fse message");
						decode_message(&*buf.unwrap_or_default(), &privkey, &list)
					};

					/* after receiving, identify peer and reply */
					let (pubkey, read_msg) = read?;
					let encoded_msg = encode_message(&msg, &pubkey,
						&privkey, &mut state.lock().unwrap().send_nonce);
					let written = write.send(encoded_msg.into()).await;
					println!("sent fse reply");
					let _ = written?;
					Ok(Some((None, pubkey, read_msg)))
				};
				future::Either::Right(acceptor_exchange_fut)
			},
		};

		/* TODO: bound buffer size to avoid DoS via OOM */
		/* subject to a 3-second timeout */
		let timeout = tokio::time::timeout(Duration::from_secs(3), do_exchange);
		let bounded_time_exchange = async {
			let result = match timeout.await {
				Ok(x) => x,
				Err(e) => Err(std::io::Error::new(std::io::ErrorKind::Other, e)),
			};
			match result {
				Err(e) => {
					println!("TCP state exchange failed: {}", e);
					Err(std::io::Error::new(std::io::ErrorKind::Other, e))
				},
				x => x,
			}
		};
		bounded_time_exchange
	}

	async fn periodic_state_exchange(&mut self) -> impl Stream<Item=Result<Option<MessageReceipt>, io::Error>> {
		/* generate stream of timer wakeups */
		let timer_ticks = IntervalStream::new(tokio::time::interval(self.params.state_exchange_delta));

		/* for each full-state message we want to send, connect to a peer and exchange state */
		let state = Arc::clone(&self.state);
		let identity = self.identity;
		let privkey = self.privkey.clone();
		timer_ticks
//			.map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e))
			.then(move |_now| {
				let state = Arc::clone(&state);
				let identity = identity;
				let privkey = privkey.clone();
				/* pick random peer, then random route to it */
				let (addr, target_id) = { /* lexical borrowck workaround */
					let locked_state = state.lock().expect("could not lock state mutex");
					let member = match locked_state.member_list.get_random_member(&identity) {
						Some(m) => m,
						None => return future::Either::Left(future::ok(None)),
					};
					let addr = match member.get_random_route() {
						Some(r) => r.addr,
						None => return future::Either::Left(future::ok(None)),
					};
					println!("queued full state-ex w/ {:?} @ {}", member, addr);
					(addr, member.identity)
				};
				future::Either::Right(
					Gossiper::exchange_memberlist(state, privkey, ExchangeSide::Initiator(target_id, addr))
				)
			})
	}

	fn open_localcommand_stream(&self) -> Result<
		FramedRead<namedpipe::AsyncFdStream,
			localcommand::LocalCommandCodec>,
		std::io::Error> {
		let fifo_path = &std::path::Path::new("/tmp/gossip-fifo");
		let _ = namedpipe::mkfifo(fifo_path); /* may already exist, who cares */
		let fifo_file = namedpipe::openfifo(fifo_path)?;
		use std::os::unix::io::IntoRawFd;
		let fifo_stream = namedpipe::AsyncFdStream::new(fifo_file.into_raw_fd())?;
		let command_stream = FramedRead::new(fifo_stream, localcommand::LocalCommandCodec);
		Ok(command_stream)
	}

	fn listen_localcommands(&self, stream: impl Stream<Item=Result<LocalCommand, io::Error>> + Unpin)
		-> impl Future<Output=()>
	{
		println!("listening for local commands...");
		let state = Arc::clone(&self.state);
		let rest = self.clone();
		stream.fold((), move |(), result_cmd| {
			match result_cmd {
				Ok(cmd) => {
					let mut state = state.lock().expect("could not lock state mutex");
					match cmd {
						LocalCommand::AddPeer(identity, addr) => {
							let mut new_member = Member::new(identity);
							new_member.seen_with_addr(addr, state.lamport_time.clone());
							println!("adding peer: {:?}", new_member);
							let new_members = MemberList::new_from(Some(new_member));
							state.member_list.merge(&new_members);
							println!("current members: {:?}", state.member_list);
						},
						LocalCommand::SetParam(param, val) => {
							println!("should set {} to \"{}\"", param, val);
						},
						LocalCommand::GetParam(param) => {
							println!("should get {}", param);
						},
						LocalCommand::Resolve(identity) => {
							let addrs = if let Some(x) = state.member_list.get(identity) {
								x.routes.iter().map(|r| r.addr).collect::<Vec<_>>()
							} else {
								vec![]
							};
							println!("{:?} @ {:?}", identity, &*addrs);
						},
						LocalCommand::SaveConfig => {
							drop(state); /* avoid deadlock, as save_config must lock state */
							let _ = save_config(&rest);
						},
						LocalCommand::ListPeers => {
							for m in state.member_list.iter() {
								println!("{:?}", m);
							}
							let _ = save_config(&rest);
						},
					};
				},
				Err(e) => {
					println!("local command error: {}", e);
				},
			};
			futures::future::ready(())
		})
	}

	fn serve<'a, S: Stream<Item=Result<(Option<SocketAddr>, PubKey, Message), io::Error>> + 'a>(&'a self, stream: S)
		-> impl Future<Output=()> + 'a
	{
		let state = Arc::clone(&self.state);
		stream.map(move |result| match result {
			Ok((addr, sender_id, msg)) => {
				println!("received from {:?}: {:?} @ {:?}", addr, msg, *state);
				let mut state = state.lock().expect("could not lock state mutex");
				state.update(self.identity, sender_id, &msg);
				/* witness addresses for the appropriate member */
				addr.and_then(|addr| state.member_list.get_mut(sender_id).map(|m| m.seen_with_addr(addr, LamportTime::new())));
			}
			_ => ()
		}).fold((), |_new, ()| async move {})
	}
}

fn decode_message(buf: &[u8], key: &PrivKey, members: &MemberList) -> Result<(PubKey, Message), io::Error> {
	let pubkeys_iter = members.iter().map(|x| &x.identity);
	let (sender_id, plaintext) = match crypto::decode(pubkeys_iter, key, buf) {
		Some(v) => v,
		None => {
			println!("could not determine sender, discarding message");
			return Err(io::Error::new(io::ErrorKind::Other, "could not identify sender (corrupt message?)"))
		},
	};

	use bincode::Options;
	let conf = bincode::options().with_limit(16384);
	match conf.deserialize(&*plaintext) {
		Ok(m) => Ok((sender_id, m)),
		Err(e) => {
			println!("bincode decode error: {}, data: {:?}", e, plaintext);
			Err(io::Error::new(io::ErrorKind::InvalidInput, e))
		},
	}
}

fn encode_message(msg: &Message, target: &PubKey, key: &PrivKey, nonce: &mut Nonce) -> Vec<u8> {
	use bincode::Options;
	let conf = bincode::options().with_limit(16384);
	let ser = match conf.serialize(&msg) {
		Ok(m) => m,
		Err(e) => {
			println!("bincode serialization error: {}", e);
			vec![]
		},
	};

	crypto::encode(target, key, nonce, &*ser)
}

struct BinaryCodec(PrivKey, Arc<Mutex<State>>);

impl tokio_util::codec::Decoder for BinaryCodec {
	type Item = (PubKey, Message);
	type Error = io::Error;

	fn decode(&mut self, buf: &mut bytes::BytesMut) -> io::Result<Option<Self::Item>> {
		if buf.len() == 0 {
			return Ok(None);
		}
		println!("udp recv {}B", buf.len());
		let state = self.1.lock().expect("could not lock state mutex");

		let result = match decode_message(buf, &self.0, &state.member_list) {
			Ok((id, msg)) => Ok(Some((id, msg))),
			Err(e) => Err(e),
		};
		use crate::bytes::Buf;
		buf.advance(buf.len());
		result
	}
}

type EncodeItem = (SocketAddr, PubKey, Message, PrivKey);

impl tokio_util::codec::Encoder<EncodeItem> for BinaryCodec {
	type Error = io::Error;

	fn encode(&mut self, (_addr, target_id, msg, privkey): EncodeItem, buf: &mut bytes::BytesMut) -> Result<(), Self::Error> {
		let mut state = self.1.lock().expect("could not lock state mutex");

		let ser = encode_message(&msg, &target_id, &privkey, &mut state.send_nonce);
		buf.extend(ser);
		Ok(())
	}
}

/*impl UdpCodec for BinaryCodec {
	type In = (Option<SocketAddr>, PubKey, Message);
	type Out = (SocketAddr, PubKey, Message, PrivKey);

	fn decode(&mut self, src: &SocketAddr, buf: &[u8]) -> io::Result<Self::In> {
		println!("udp recv {}B", buf.len());
		let state = self.1.lock().expect("could not lock state mutex");

		match decode_message(buf, &self.0, &state.member_list) {
			Ok((id, msg)) => Ok((Some(*src), id, msg)),
			Err(e) => Err(e),
		}
	}

	fn encode(&mut self, (addr, target_id, msg, privkey): Self::Out, buf: &mut Vec<u8>) -> SocketAddr {
		let mut state = self.1.lock().expect("could not lock state mutex");

		let ser = encode_message(&msg, &target_id, &privkey, &mut state.send_nonce);
		buf.extend(ser);
		addr
	}
}*/

pub fn save_config(g: &Gossiper) -> io::Result<()> {
	use std::io::Write;
	use std::fs::OpenOptions;
	let path = match g.config_path {
		Some(ref p) => p,
		None => return Ok(()),
	};
	let mut file = OpenOptions::new().write(true).create(true).open(path)?;
	file.write_all(&*serde_yaml::to_string(&g).unwrap().as_bytes())?;
	Ok(())
}

pub fn main(config_path: &std::path::Path) {
	let mut g = match std::fs::File::open(&config_path) {
		Ok(mut f) => {
			use std::io::Read;
			let mut s = String::new();
			f.read_to_string(&mut s).expect("failed to read configuration");
			serde_yaml::from_str(&*s).expect("failed to parse configuration")
		},
		Err(ref e) if e.kind() == io::ErrorKind::NotFound => {
			let addr = "0.0.0.0:2115".parse().expect("invalid local address");
			let (pubkey, privkey) = crypto::gen_keys();
			Gossiper::new(pubkey, privkey, addr)
		},
		Err(e) => {
			println!("error opening configuration: {}", e);
			std::process::exit(1)
		},
	};

	/* save initial configuration (e.g. after generating keypair on first run) */
	g.config_path = Some(config_path.into());
	let _ = save_config(&g);

	let mut event_loop = Runtime::new().expect("failed to create tokio core");

	g.run(&mut event_loop);
}
