extern crate libc;

use std;
use std::ffi::CString;
use std::path::Path;

use std::os::unix::ffi::OsStrExt;
use std::os::unix::io::FromRawFd;

pub fn mkfifo(filename: &Path) -> Result<(), std::io::Error> {
	let c_filename = CString::new(filename.as_os_str().as_bytes())
		.expect("invalid filename for mkfifo");

	let out = if unsafe { libc::mkfifo(c_filename.as_ptr(), 0o644) < 0 } {
		Err(std::io::Error::last_os_error())
	} else {
		Ok(())
	};

	drop(c_filename);
	out
}

pub fn openfifo(filename: &Path) -> Result<std::fs::File, std::io::Error> {
	let c_filename = CString::new(filename.as_os_str().as_bytes())
		.expect("invalid filename for mkfifo");

	let raw_fd = unsafe {
		libc::open(c_filename.as_ptr(), libc::O_RDWR | libc::O_NONBLOCK)
	};

	if raw_fd < 0 {
		return Err(std::io::Error::last_os_error());
	}

	let file = unsafe {
		FromRawFd::from_raw_fd(raw_fd)
	};

	drop(c_filename);
	Ok(file)
}


use futures::ready;
use std::io::{self, Read, Write};
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::io::{AsyncRead, AsyncWrite};
use tokio::io::unix::AsyncFd;
use std::os::unix::prelude::RawFd;

pub struct AsyncFdStream {
	inner: AsyncFd<RawFd>,
}

impl AsyncFdStream {
	pub fn new(fd: RawFd) -> io::Result<Self> {
		Ok(Self {
			inner: AsyncFd::new(fd)?,
		})
	}
}

/*impl io::Read for AsyncFdStream {
	fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
		self.io.get_mut().read(buf)
	}
}

impl io::Write for AsyncFdStream {
	fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
		self.io.get_mut().write(buf)
	}
	fn flush(&mut self) -> io::Result<()> {
		self.io.get_mut().flush()
	}
}*/

impl AsyncRead for AsyncFdStream {
	fn poll_read(
	   mut self: Pin<&mut Self>,
	   cx: &mut Context,
	   buf: &mut tokio::io::ReadBuf
	) -> Poll<Result<(), io::Error>> {
		loop {
			let mut guard = futures::ready!(self.inner.poll_read_ready_mut(cx))?;

			match guard.try_io(|inner| {
				let mut file = unsafe { std::fs::File::from_raw_fd(inner.get_ref().clone()) };
				let result = file.read(buf.initialize_unfilled());
				std::mem::forget(file);
				result
			}) {
				Ok(result) => match result {
					Ok(count) => {
						buf.advance(count);
						return Poll::Ready(Ok(()))
					},
					Err(e) => {
						return Poll::Ready(Err(e))
					},
				}
				Err(_would_block) => continue,
			}
		}
	}
}

impl AsyncWrite for AsyncFdStream {
	fn poll_write(
		self: Pin<&mut Self>,
		cx: &mut Context<'_>,
		buf: &[u8]
	) -> Poll<io::Result<usize>> {
		loop {
			let mut guard = ready!(self.inner.poll_write_ready(cx))?;

			match guard.try_io(|inner| {
				let mut file = unsafe { std::fs::File::from_raw_fd(inner.get_ref().clone()) };
				let result = file.write(buf);
				std::mem::forget(file);
				result
			}) {
				Ok(result) => return Poll::Ready(result),
				Err(_would_block) => continue,
			}
		}
	}

	fn poll_flush(
		self: Pin<&mut Self>,
		_cx: &mut Context<'_>,
	) -> Poll<io::Result<()>> {
		Poll::Ready(Ok(()))
	}

	fn poll_shutdown(
		self: Pin<&mut Self>,
		_cx: &mut Context<'_>,
	) -> Poll<io::Result<()>> {
		//self.inner.get_ref().shutdown(std::net::Shutdown::Write)?;
		Poll::Ready(Ok(()))
	}
}
