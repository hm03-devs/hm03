extern crate rand;

use crate::crypto::PubKey;
use crate::member::Member;

use std;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Default)]
pub struct MemberList(Vec<Member>);

impl MemberList {
	pub fn new() -> MemberList {
		MemberList(vec![])
	}
	pub fn new_from<I: IntoIterator<Item=Member>>(iter: I) -> MemberList {
		MemberList(iter.into_iter().collect())
	}
	pub fn get_mut(&mut self, identity: PubKey) -> Option<&mut Member> {
		self.0.iter_mut().find(|m| m.identity == identity)
	}

	pub fn get(&self, identity: PubKey) -> Option<&Member> {
		self.0.iter().find(|m| m.identity == identity)
	}

	pub fn add(&mut self, m: Member) {
		self.0.push(m)
	}

	pub fn merge(&mut self, other: &MemberList) {
		for new_m in other {
			/* try to update entry in list if it exists */
			if let Some(old_m) = self.get_mut(new_m.identity) {
				old_m.merge_routes(&*new_m.routes);
				continue;
			}
			/* if member not in list, add it */
			self.add(new_m.clone());
		}
	}

	/** randomly select `n` members. returns fewer if fewer are present. */
	pub fn get_random_members(&self, skip: &PubKey, n: usize) -> Vec<&Member> {
		use rand::seq::IteratorRandom;
		self.0.iter().filter(|x| x.identity != *skip)
			.choose_multiple(&mut rand::thread_rng(), n)
	}

	/** get a random member, or none if there are no members */
	pub fn get_random_member(&self, skip: &PubKey) -> Option<&Member> {
		self.get_random_members(skip, 1).into_iter().next()
	}
}

impl MemberList {
	pub fn iter(&self) -> std::slice::Iter<Member> {
		self.into_iter()
	}
}

impl <'a> IntoIterator for &'a MemberList {
	type Item = &'a Member;
	type IntoIter = std::slice::Iter<'a, Member>;
	fn into_iter(self) -> std::slice::Iter<'a, Member>/*impl Iterator<Item=&'a Member>*/ {
		self.0.iter()
	}
}
